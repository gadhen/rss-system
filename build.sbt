name := "rss-system"

organization in ThisBuild := "com.gadhen"

version in ThisBuild:= "1.0"

scalaVersion in ThisBuild:= "2.10.1"

libraryDependencies in ThisBuild ++= Seq(
	"org.scalatest" %% "scalatest" % "1.9.1" % "test"
)

EclipseKeys.createSrc in ThisBuild := EclipseCreateSrc.Default + EclipseCreateSrc.Resource



