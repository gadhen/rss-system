name := "rss-system-dao"

libraryDependencies ++= Seq(
	"com.typesafe.slick" %% "slick" % "1.0.1",
	"org.xerial" % "sqlite-jdbc" % "3.7.2",
	"commons-codec" % "commons-codec" % "1.6"
)
