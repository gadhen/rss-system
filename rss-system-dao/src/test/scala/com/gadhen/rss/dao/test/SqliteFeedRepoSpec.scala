package com.gadhen.rss.dao.test

import org.scalatest.FunSuite
import com.gadhen.rss.dao.sqlite.SqliteFeedRepo
import com.gadhen.rss.model.Feed

class SqliteFeedRepoSpec extends FunSuite {

  test("repo should register new feed and find it by title") {
    val f = new Feed
    f.url = "test_test" + (new java.util.Date()).getTime()
    f.title = "lol omg"
    f.desc = "long text here"
    SqliteFeedRepo.registerFeed(f)

    SqliteFeedRepo.findByTitle(f.title).foreach { f =>

      println(f.id + ": " + f.url + ": " + f.lastPublished + ": " + f.desc)

    }
  }

}