package com.gadhen.rss.dao.test

import org.scalatest.FunSuite
import com.gadhen.rss.dao.utils.Utils
import java.util.Calendar

class UtilsSpec extends FunSuite {

  val url1 = "www.idontcare.lol"
  val url2 = "www.www.www"
  test("utils should create sha1 hash") {
    var hash = ""
    hash = Utils.getSha256Hash(url1, new java.util.Date)
    println(hash + "::: " + hash.length)
    assert(hash.length === 64)
  }

  test("sha1 hash should be unique") {
    val sameDate = new java.util.Date
    val hashUrl1SameDate = Utils.getSha256Hash(url1, sameDate)
    val hashUrl2SameDate = Utils.getSha256Hash(url2, sameDate)
    assert(hashUrl1SameDate != hashUrl2SameDate)
    val cal = Calendar.getInstance()
    val d1 = cal.getTime()
    cal.add(Calendar.DATE, 1)
    val d2 = cal.getTime()
    val hashUrlDiffDate1 = Utils.getSha256Hash(url1, d1)
    val hashUrlDiffDate2 = Utils.getSha256Hash(url1, d2)
    assert(hashUrlDiffDate1 != hashUrlDiffDate2)
    assert(hashUrlDiffDate1 != hashUrl1SameDate)
    assert(hashUrlDiffDate2 != hashUrl1SameDate)
    assert(hashUrlDiffDate1 != hashUrl2SameDate)
    assert(hashUrlDiffDate2 != hashUrl2SameDate)
  }

}