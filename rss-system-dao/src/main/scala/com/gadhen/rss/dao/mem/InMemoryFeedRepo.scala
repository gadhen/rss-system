package com.gadhen.rss.dao.mem

import scala.collection.Seq
import com.gadhen.rss.model.FeedRepo
import com.gadhen.rss.model.Feed
import com.gadhen.rss.model.FeedItem

object InMemoryFeedRepo extends FeedRepo {
  val memStorage: scala.collection.mutable.Buffer[Feed] = scala.collection.mutable.Buffer[Feed]()
  val testf = new Feed
  testf.url = getClass.getResource("/testfeedatom.xml").getFile()
  testf.title = "EQD"
  registerFeed(testf)
  var nextIndex = 0
  def findByTitle(title: String) = {
    memStorage.filter(feed => feed.title == title)
  }

  def findById(id: Long) = {
    memStorage.find(feed => feed.id == id)
  }

  def findByUrl(url: String) = {
    memStorage.find(feed => feed.url == url)
  }

  def registerFeed(feed: Feed) = {
    feed.id = nextIndex
    nextIndex = nextIndex + 1
    memStorage += feed
    Some(feed)
  }

  def addNewItems(feed: Feed, newItems: Seq[FeedItem]): Unit = {
    val oldf = memStorage.find(f => f.id == feed.id)
    if (oldf != None) {
      val f = oldf.get
      f.items ++= newItems
      memStorage.update(memStorage.indexOf(f), f)
    }
  }

  def getAll(): Seq[Feed] = memStorage
  
  def insertFeedItem(feedId: Long, newItem: FeedItem) = {
    val oldf = findById(feedId)
    if(oldf != None) {
      val f = oldf.get
      f.items ++= Seq(newItem)
      memStorage.update(memStorage.indexOf(f), f)
    }
  }

}
