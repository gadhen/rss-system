package com.gadhen.rss.dao.config

object Config {

  val dbUrl = "jdbc:sqlite:" + scala.util.Properties.envOrElse("RSS_SYSTEM_DB_URL", "");

  val driver = "org.sqlite.JDBC"

}