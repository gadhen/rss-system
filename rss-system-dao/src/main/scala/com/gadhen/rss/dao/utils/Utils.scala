package com.gadhen.rss.dao.utils

import org.apache.commons.codec.CharEncoding
import org.apache.commons.codec.binary.Hex

object Utils {

  private val md = java.security.MessageDigest.getInstance("SHA-256");
  def getSha256Hash(url: String, publishDate: java.util.Date): String = {
    md.reset()
    md.update(url.getBytes("UTF-8"))
    md.update(publishDate.getTime().toString().getBytes("UTF-8"))
    Hex.encodeHexString(md.digest())
  }

}