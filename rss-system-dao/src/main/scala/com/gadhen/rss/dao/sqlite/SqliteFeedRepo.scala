package com.gadhen.rss.dao.sqlite

import scala.collection.Seq
import com.gadhen.rss.model.FeedRepo
import scala.slick.session.Database
import scala.slick.lifted.{ BaseTypeMapper, TypeMapperDelegate }
import scala.slick.jdbc.{ GetResult, SetParameter, StaticQuery => Q }
import Database.threadLocalSession
import com.gadhen.rss.model.Feed
import com.gadhen.rss.dao.config.Config
import com.gadhen.rss.model.FeedItem
import com.gadhen.rss.dao.utils.Utils
import com.gadhen.rss.model.FeedItem

object SqliteFeedRepo extends FeedRepo{

  val registerFeedSql = "INSERT INTO FEEDS (URL, TITLE, DESCRIPTION) VALUES (?, ?, ?)"

  val selectFeedByTitle = "SELECT * FROM FEEDS WHERE TITLE = ?"
  
  val selectFeedByUrl = "SELECT * FROM FEEDS WHERE URL = ?"
    
  val selectFeedById = "SELECT * FROM FEEDS WHERE ID = ?"
    
  val insertFeedItem = "INSERT INTO FEEDITEMS (HASH, URL, TITLE, DESCRIPTION, AUTHOR, CATEGORY, PUBLISHED, READ, FEED_ID, CONTENT)" +
		  " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
  
  val selectAllFeeds = "SELECT * FROM FEEDS"
    
  val selectFeedItemsForFeedIdOrderByDateAsc = "SELECT * FROM FEEDITEMS WHERE FEED_ID = ? ORDER BY PUBLISHED ASC"
    
  val selectFeedItemForFeedIdAndHash = "SELECT * FROM FEEDITEMS WHERE FEED_ID = ? AND HASH = ?"
    
  val updateLastPublishedValue = "UPDATE FEEDS SET LAST_PUBLISHED = " +
  		"(SELECT MAX(PUBLISHED) FROM FEEDITEMS WHERE FEEDITEMS.FEED_ID = FEEDS.ID) " +
  		"WHERE FEEDS.ID = ?"

  val database = Database.forURL(Config.dbUrl, driver = Config.driver)

  implicit val getFeedResult: GetResult[Feed] = GetResult { r =>
    val f = new Feed
    f.id = r.nextLong
    f.url = r.rs.getString("URL")
    f.title = r.rs.getString("TITLE")
    f.desc = r.rs.getString("DESCRIPTION")
    val dateNumber = Option(r.rs.getLong("LAST_PUBLISHED"))
    if (dateNumber != None) {
      f.lastPublished = new java.util.Date(dateNumber.get)
    }

    f //TODO: Ugly return statement	  
  }
  
    implicit val getFeedItemResult: GetResult[FeedItem] = GetResult { r =>
    val fi = new FeedItem
    fi.hash = r.nextString
    //no content and description for memory reasons

    fi //TODO: Ugly return statement	  
  }

  override def registerFeed(feed: Feed): Option[Feed] = {
    database withTransaction {
      Q.update[(String, String, String)](registerFeedSql).execute((feed.url, feed.title, feed.desc))
    }
    Some(feed)
  }

  override def findByTitle(title: String): Seq[Feed] = {
    database withTransaction {
      Q.query[(String), Feed](selectFeedByTitle).list(title)
    }
  }
  override def insertFeedItem(feedId: Long, feedItem: FeedItem) = {
    val hash = Utils.getSha256Hash(feedItem.url, feedItem.published)
    var categoryString = feedItem.category.flatMap {
      _ match {
        case x: String if !x.nonEmpty => None
        case x: String => Some(x)
        case _ => None
      }
    }.mkString("|")
    
    database withTransaction {
      Q.update[(String, String, String, String, String, String, Long, Boolean, Long, String)](insertFeedItem).execute((hash, feedItem.url, feedItem.title, feedItem.description, feedItem.author, categoryString, feedItem.published.getTime(), feedItem.read, feedId, feedItem.content))
    }
  }
  
  override def findByUrl(url: String): Option[Feed] = {
    database withTransaction {
      Q.query[(String), Feed](selectFeedByUrl).firstOption(url)
    }
  }

  override def findById(id: Long): Option[Feed] = {
    database withTransaction {
      Q.query[(String), Feed](selectFeedById).firstOption(id+"")
    }
  }
  
  override def getAll() : Seq[Feed] = {
    database withTransaction {
      Q.queryNA[Feed](selectAllFeeds).list()
    }
  }

  def addNewItems(feed: Feed, newItems: Seq[FeedItem]): Unit = {
    database withTransaction {
      val oldf = findById(feed.id)
      if (oldf != None) {
        val f = oldf.get
        f.items ++= newItems
        newItems.foreach { ni =>
          insertFeedItem(feed.id, ni)          
        }
      }
    }
  }
  
  def fetchItemsForFeedId(feedId: Long): Seq[FeedItem] = {
    database withTransaction {
      Q.query[(Long), FeedItem](selectFeedItemsForFeedIdOrderByDateAsc).list(feedId)
    }
  }
  
  def feedItemExists(feedId: Long, hash: String): Boolean = {
    database withTransaction {
      val fi = Q.query[(Long, String), FeedItem](selectFeedItemForFeedIdAndHash).firstOption(feedId, hash)
      if(fi != None) {
        true
      }
      else {
        false
      }
    }
  }

  def setLastPublishedDate(feedId: Long) = {
    database withTransaction {
      Q.update[(Long)](updateLastPublishedValue).execute(feedId)
    }
  }
}