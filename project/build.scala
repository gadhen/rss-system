import sbt._
import Keys._

object RssSystem extends Build {

	val appVersion = "1.0"
	
    lazy val root = Project(id = "rss-system",
                            base = file(".")) aggregate(parser,model,dao,fetcher)

    lazy val parser = Project(id = "rss-system-parser",
                           base = file("rss-system-parser")) dependsOn(model)
						   
	lazy val model = Project(id = "rss-system-model",
                           base = file("rss-system-model"))
						   
	lazy val dao = Project(id = "rss-system-dao",
                           base = file("rss-system-dao")) dependsOn(model)
						   
	lazy val fetcher = Project(id = "rss-system-fetcher",
								base = file("rss-system-fetcher")) dependsOn(model,dao,parser)
}