addSbtPlugin("com.typesafe.sbteclipse" %% "sbteclipse-plugin" % "2.2.0")

// TestNG plugin
// addSbtPlugin("de.johoop" % "sbt-testng-plugin_2.10.1" % "2.0.3")

// sbt-dependency-graph - Visualize your project's dependencies. - https://github.com/jrudolph/sbt-dependency-graph
// addSbtPlugin("net.virtual-void" %% "sbt-dependency-graph" % "0.7.3")

resolvers += "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases/"


