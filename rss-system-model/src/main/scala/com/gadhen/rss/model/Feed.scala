package com.gadhen.rss.model

case class Feed(var id: Long = -1, var title: String,
    var url: String,
    var desc: String,
    var items: Seq[FeedItem]) {

  var lastPublished: java.util.Date = null

  def this() = this(-1, "", "", "", List.empty)
  def this(title: String, url: String, desc: String) = this(-1, title, url, desc, List.empty)
}

trait FeedRepo {
  def findByTitle(title: String): Seq[Feed]
  def findByUrl(url: String): Option[Feed]
  def findById(id: Long): Option[Feed]
  def registerFeed(feed: Feed): Option[Feed]
  def addNewItems(feed: Feed, newItems: Seq[FeedItem])
  def getAll(): Seq[Feed]
  def insertFeedItem(feedId: Long, feedItem: FeedItem)
  def fetchItemsForFeedId(feedId: Long): Seq[FeedItem]
  def feedItemExists(feedId: Long, hash: String): Boolean
  def setLastPublishedDate(feedId: Long)
}