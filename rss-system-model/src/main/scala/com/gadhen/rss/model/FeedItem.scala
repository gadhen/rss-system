package com.gadhen.rss.model

case class FeedItem(
    var hash: String,
    var title: String,
    var url: String,
    var description: String,
    var author: String,
    var category: Seq[String] = List.empty,
    var read: Boolean = false,
    var published: java.util.Date,
    var content: String) {
  def this() = this("", "", "", "", "", List.empty, false, null,"")
}

