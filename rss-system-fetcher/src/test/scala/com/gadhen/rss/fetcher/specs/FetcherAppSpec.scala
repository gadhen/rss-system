package com.gadhen.rss.fetcher.specs

import org.scalatest.FunSuite
import com.gadhen.rss.dao.mem.InMemoryFeedRepo
import com.gadhen.rss.fetcher.FetcherApp
import com.gadhen.rss.dao.config.Config
import com.gadhen.rss.parser.FeedParser

class FetcherAppSpec extends FunSuite {

  test("should update items in feed") {
    val a = InMemoryFeedRepo.findById(0)
    assert(a.get.items.size === 0)
    FetcherApp.main(Array("http://feeds.feedburner.com/EquestriaDaily"))
    FetcherApp.updateFeedItems(a.get)
    val b = InMemoryFeedRepo.findById(0)
    assert(b.get.items.size === 25)

    println(Config.dbUrl)
  }
}