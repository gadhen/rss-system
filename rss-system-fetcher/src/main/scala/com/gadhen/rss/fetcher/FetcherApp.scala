package com.gadhen.rss.fetcher

import com.gadhen.rss.model.FeedRepo
import com.gadhen.rss.dao.mem.InMemoryFeedRepo
import com.gadhen.rss.model.Feed
import com.gadhen.rss.parser.FeedParser
import com.gadhen.rss.model.FeedItem
import com.gadhen.rss.dao.sqlite.SqliteFeedRepo
import com.gadhen.rss.dao.utils.Utils

object FetcherApp {

  val repo: FeedRepo = SqliteFeedRepo

  def registerFeed(url: String, title: String): Option[Feed] = {
    if (repo.findByUrl(url) == None) {
      val nf = new Feed()
      nf.url = url
      nf.title = title
      repo.registerFeed(nf)
    } else {
      None
    }
  }

  def updateFeedItems(feed: Feed): Unit = {
    val parsedFeed = FeedParser.parseFeedSource(feed.url)
    var newItems: Seq[FeedItem] = List.empty

    if (feed.lastPublished != null) {
      parsedFeed.items.filter(p => !p.published.before(feed.lastPublished)).foreach { parsedItem =>

        if (!repo.feedItemExists(feed.id, Utils.getSha256Hash(parsedItem.url, parsedItem.published))) {
          newItems ++= Seq(parsedItem)
        }
      }
    }
    else {
      newItems ++= parsedFeed.items
    }
    if (!newItems.isEmpty) {
      repo.addNewItems(feed, newItems)
      repo.setLastPublishedDate(feed.id)
    }
  }

  def main(args: Array[String]) {
    args.foreach { a =>
      if (a.startsWith("http://")) {
        registerFeed(a, a)
      }
    }
    repo.getAll.foreach { f =>
      updateFeedItems(f)
    }
  }
}