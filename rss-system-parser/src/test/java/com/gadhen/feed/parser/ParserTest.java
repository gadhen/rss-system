//package com.gadhen.feed.parser;
//
//import java.util.Enumeration;
//import java.util.List;
//
//import org.horrabin.horrorss.RssFeed;
//import org.horrabin.horrorss.RssItemBean;
//import org.horrabin.horrorss.RssParser;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//import com.gadhen.feed.Feed;
//import com.gadhen.feed.item.CategorizedItemBean;
//import com.gadhen.feed.parser.CategoryModuleParser;
//
//
//public class ParserTest {
//
//	private static final String CAT_RSS = "catRss";
//
//	@Test
//	public void itCanParseAtomFeed() throws Exception {
//		String feed = "src/test/resources/testfeedatom.xml";
//		RssParser rss = new RssParser(feed);
//		testParser(rss, feed);
//	}
//
//	@Test
//	public void itCanParseRssFeed() throws Exception {
//		String feed = "src/test/resources/testfeedrss.xml";
//		RssParser rss = new RssParser(feed);
//		testParser(rss, feed);
//	}
//	
//	@Test public void itCanParseAtomFeedFromWeb() throws Exception {
//		String feed = "http://feeds.feedburner.com/EquestriaDaily";
//		RssParser rss = new RssParser(feed);
//		testParser(rss, feed);
//	}
//	
//	@Test
//	public void itCanParseAtomToModel() throws Exception {
//		String feed = "src/test/resources/testfeedatom.xml";
//		Feed f= RawFeedToModelParser.parseRawFeed(feed);
//		Assert.assertNotNull(f);
//		Assert.assertFalse(f.getItems().isEmpty());
//		Assert.assertFalse(f.getItems().get(0).getCategories().isEmpty());
//	}
//	
//	private String printData(String name, String content) {
//		return String.format("%s : %s ",name, content);
//	}
//
//	private void testParser(RssParser rss, String feed) throws Exception {
//		System.out.println(feed);
//		rss.addRssModuleParser(CAT_RSS, new CategoryModuleParser());
//		boolean testcat = false;
//		RssFeed obj = rss.load();
//		List<RssItemBean> items = obj.getItems();
//		for (int i = 0; i < items.size(); i++) {
//			RssItemBean item = items.get(i);
//			CategorizedItemBean cat = (CategorizedItemBean) item.getAdditionalInfo(CAT_RSS);
//			System.out.print(printData("Title",item.getTitle()));
//			System.out.print(printData("Author",item.getAuthor()));
//			System.out.print(printData("Link",item.getLink()));
//			System.out.print(printData("Description",item.getDescription()));
//			if(cat.getCategories().hasMoreElements()) {
//				testcat = true;
//			}
//			while(cat.getCategories().hasMoreElements()) {
//				System.out.println("  Cat: " + cat.getCategories().nextElement());
//			}
//		}
//		Assert.assertNotNull(testcat);
//		Assert.assertTrue(testcat, "There are categories");
//	}
//
//}
