package com.gadhen.feed.parser

import org.scalatest.FunSuite
import scala.collection.mutable.Stack
import com.gadhen.rss.parser.FeedParser
import scala.io.Source
import java.net.URL

class FeedParserSpecs extends FunSuite{
 
  test("Feed should have items in Atom") {
   val testData = getClass.getResource("/testfeedatom.xml"); 
   val feed = FeedParser.parseFeedSource(testData.getFile())
   assert(feed.items.size === 25)
   feed.items.foreach { item =>
     print(item.title)
     item.category.foreach { cat=>
       	print(" | " + cat)
       }
     println()
   }
  }
  
  test("Feed should have items in RSS") { 
   val testData = getClass.getResource("/testfeedrss.xml"); 
   val feed = FeedParser.parseFeedSource(testData.getFile())
   assert(feed.items.size === 46)
   feed.items.foreach { item =>
     print(item.title)
     item.category.foreach { cat=>
       	print(" | " + cat)
       }
     println()
   }
  }  
}