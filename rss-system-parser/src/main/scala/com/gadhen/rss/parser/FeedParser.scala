package com.gadhen.rss.parser

import org.horrabin.horrorss.RssParser
import scala.collection.JavaConversions._
import java.net.URL
import com.gadhen.rss.model.Feed
import com.gadhen.feed.parser.CategoryModuleParser
import com.gadhen.feed.item.CategorizedItemBean
import com.gadhen.rss.model.FeedItem
import com.gadhen.feed.parser.BetterDateParser

object FeedParser {
  val CAT_RSS = "catRss"
  def parseFeedSource(feedSource: String): Feed = {
    val rss = new RssParser(feedSource)
    rss.setDateParser(new BetterDateParser());
    parseFeed(rss)

  }

  private def parseFeed(rss: org.horrabin.horrorss.RssParser): Feed = {
    rss.addRssModuleParser(CAT_RSS, new CategoryModuleParser())
    val obj = rss.load()
    val f = new Feed(obj.getChannel().getTitle(), obj.getChannel().getLink(), obj.getChannel().getDescription())
    obj.getItems().foreach { raw =>
      val item = new FeedItem()
      item.author = raw.getAuthor()
      item.description = raw.getDescription()
      item.title = raw.getTitle()
      item.url = raw.getLink()
      item.published = raw.getPubDate()
      val cat = raw.getAdditionalInfo(CAT_RSS).asInstanceOf[CategorizedItemBean]
      while (cat.getCategories().hasMoreElements()) {
        item.category ++= Seq(cat.getCategories().nextElement()) //TODO: inefficent! immutable list
      }
      f.items ++= Seq(item) //TODO inefficent
      if (f.lastPublished == null) {
        f.lastPublished = item.published
      } else if (item.published.after(f.lastPublished)) {
        f.lastPublished = new java.util.Date(item.published.getTime()) //TODO slow, but needed to copy date
      }
    }

    f //TODO: ugly return statement
  }
}