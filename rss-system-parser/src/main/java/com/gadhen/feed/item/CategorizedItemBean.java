package com.gadhen.feed.item;

import java.util.Enumeration;

public class CategorizedItemBean{
	
	private Enumeration<String> categories;

	public CategorizedItemBean() {
		super();
	}

	public CategorizedItemBean(Enumeration<String> category) {
		super();
		this.categories = category;
	}

	public Enumeration<String> getCategories() {
		return categories;
	}

	public void setCategory(Enumeration<String> category) {
		this.categories = category;
	}
}
