//package com.gadhen.feed.parser;
//
//import java.util.List;
//
//import org.horrabin.horrorss.RssFeed;
//import org.horrabin.horrorss.RssItemBean;
//import org.horrabin.horrorss.RssParser;
//
//import com.gadhen.feed.Feed;
//import com.gadhen.feed.FeedItem;
//import com.gadhen.feed.item.CategorizedItemBean;
//
//public class RawFeedToModelParser {
//
//	private static final String CAT_RSS = "catRss";
//
//	public static Feed parseRawFeed(String feedSource) throws Exception {
//		Feed f = new Feed();
//		RssParser rss = new RssParser(feedSource);
//		rss.addRssModuleParser(CAT_RSS, new CategoryModuleParser());
//		RssFeed obj = rss.load();
//		f.setName(obj.getChannel().getTitle());
//		List<RssItemBean> items = obj.getItems();
//		for (RssItemBean item : items) {
//			FeedItem fi = new FeedItem();
//			fi.setTitle(item.getTitle());
//			fi.setDescrition(item.getDescription());
//			fi.setLink(item.getLink());
//			fi.setPublishedOn(item.getPubDate());
//			CategorizedItemBean cat = (CategorizedItemBean) item.getAdditionalInfo(CAT_RSS);
//			while(cat.getCategories().hasMoreElements()) {
//				fi.getCategories().add(cat.getCategories().nextElement());
//			}
//			f.getItems().add(fi);
//		}
//		return f;
//	}
//}
