package com.gadhen.feed.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.horrabin.horrorss.RssParser;
import org.horrabin.horrorss.util.DateParser;

public class BetterDateParser implements DateParser {
	@Override
	public Date getDate(String date, int rssType) throws Exception {
		Date res = null;
		String pattern = null;

		pattern = getPattern(date, rssType);

		try {
			res = parseDate(date, pattern);
		} catch (java.text.ParseException e) {
			int[] types = { RssParser.TYPE_RDF, RssParser.TYPE_RSS,
					RssParser.TYPE_ATOM };
			for (int i : types) {
				if (i != rssType) {
					try {
						res = parseDate(date, getPattern(date, i));
						break;
					} catch (java.text.ParseException ex) {
						continue;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error parsing date: " + date + " [Type: "
					+ rssType + "] --" + e.toString());
			// throw e;
		}

		return res;
	}

	private String getPattern(String date, int rssType) {
		switch (rssType) {
		case RssParser.TYPE_RDF: {
			if (date.indexOf("+") >= 0)
				return "yyyy-MM-dd'T'HH:mm:ss+HH:mm";
			return "yyyy-MM-dd'T'HH:mm:ss-HH:mm";
		}
		case RssParser.TYPE_RSS: {
			return "EEE, dd MMM yyyy HH:mm:ss Z";
		}
		case RssParser.TYPE_ATOM: {
			return "yyyy-MM-dd'T'HH:mm:ss";
		}
		}
		return "yyyy-MM-dd";
	}

	private Date parseDate(String date, String pattern) throws ParseException {
		Date res;
		SimpleDateFormat sd = new SimpleDateFormat(pattern, Locale.ENGLISH);
		res = sd.parse(date);
		return res;
	}

}
