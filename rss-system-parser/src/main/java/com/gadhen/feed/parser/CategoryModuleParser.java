package com.gadhen.feed.parser;

import java.util.Enumeration;

import org.horrabin.horrorss.RssModuleParser;

import com.gadhen.feed.item.CategorizedItemBean;
import com.hp.hpl.sparta.Document;

public class CategoryModuleParser implements RssModuleParser{

	@Override
	public Object parseChannel(int arg0, Document arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object parseImage(int arg0, Document arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object parseItem(int rssType, Document doc, int index) throws Exception {
		CategorizedItemBean cat = new CategorizedItemBean(); 
	    try { 
	      Enumeration<String> category = doc.xpathSelectStrings("rss/channel/item[" + index + "]//category/@term");
	      if(!category.hasMoreElements() ){
	    	  category = doc.xpathSelectStrings("feed/entry[" + index + "]//category/@term");
	      }
	      cat.setCategory(category);
	    }catch(Exception e){ 
	      throw new Exception("Error CategoryRSS Module item at index " + index, e); 
	    } 

	    return cat;
	}


}
